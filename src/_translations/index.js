import en from "./en.json";
import de from "./de.json";

const langs = {
  en,
  de
};

export default function (lang = "en") {
  return langs[lang];
};