import { requiredField, minLength, maxLength, min, max } from './default_validation'
import { notEqual } from 'assert';


/**
 * Method search for all inputs in .form-serializer with 'data-field-name' attribute, next asking API for data and fill input based on response.
 * @param {Axios} axios client should be used for asking API for form data 
 * @param {id} id it's define id field in response
 * @param {entityName} entityName define entity field in response
 */
export function getForm(axios, id, entityName) {

    return new Promise((resolve, reject) => {
        const fields = [];
        const inputList = [];

        let form = document.querySelector('.form_serializer');
        let inputs = form.getElementsByTagName('input');
    for (var index = 0; index < inputs.length; ++index) {
        let input = inputs[index].getAttribute('data-field-name');
        if (input != undefined) {
            inputList.push(inputs[index]);
        }
    }

    let selects = form.getElementsByTagName('select');
    for (var index = 0; index < selects.length; ++index) {
        let input = selects[index].getAttribute('data-field-name');
        if (input != undefined) {
            inputList.push(selects[index]);
        }
    }

    inputList.forEach(input => {
        const field = input.getAttribute('data-field-name')
        fields.push(field)
    })

      axios.post('/dynamic/retrieve', {
          id: id,
          entity: entityName,
          fields: [...fields]
      }).then(response => {
          response.forEach(element => {
              parseForm(inputList, element['fields']);
          })
          resolve(response)
      })
    });  
}

/**
 * Method search for all inputs (and selects) in .form-serializer with 'data-field-name' attribute, next validate it and after successful (empty error list) send data via API.
 * @param {Axios} axios client should be used for save inputs value 
 * @param {id} id OPTIONAL! it's define id field in response, null means creating new entity object 
 * @param {entityName} entityName define entity field in response
 * @param validationFunctions contains function in format function <name> {} for validation. Single function should return object using isValidObject.
 * @param languageCode contain information about language code. It's used to create message with correctly language. @see {default_validation} validators
 */

export function sendForm(axios, id, entityName, validationFunctions, languageCode) {
    return new Promise((resolve, reject) => {
        const inputList = [];

        let form = document.querySelector('.form_serializer');
        let inputs = form.getElementsByTagName('input');
        for (var index = 0; index < inputs.length; ++index) {
            let type = inputs[index].type;
            if (type == 'radio' ) {
                let input = inputs[index].getAttribute('data-field-name');
                let checked = inputs[index].checked
                if (input && checked) {
                    inputList.push(inputs[index])
                }
            } else if(type == 'checkbox'){
                let input = inputs[index].getAttribute('data-field-name');
                if(input){
                    inputList.push(inputs[index])
                }
            }
            else {
                let input = inputs[index].getAttribute('data-field-name');
    
                if (input != undefined) {
                    inputList.push(inputs[index]);
                }
            }
        }
    
        let selects = form.getElementsByTagName('select');
        for (var index = 0; index < selects.length; ++index) {
            let input = selects[index].getAttribute('data-field-name');
            if (input) {
                inputList.push(selects[index])
            }
        }
    
    
        const errorList = [];
    
        //Default validators
        inputList.forEach(input => {
            if (input.type == 'text') {
                checkRequired(errorList, input, languageCode)
                checkTextMinLength(errorList, input, languageCode)
                checkTextMaxLength(errorList, input, languageCode)
            } else if (input.type == 'number') {
                checkRequired(errorList, input, languageCode)
                checkMinValue(errorList, input, languageCode);
                checkMaxValue(errorList, input, languageCode);
            }
        })
    
        //Custom validation check
        inputList.forEach(input => {
            const requiredValidation = input.getAttribute('validation');
            if (requiredValidation) {
                const validate = validationFunctions.find(fun => fun.name == requiredValidation)
                if (validate) {
                    const result = validate(input)
                    if (result.error) {
                        errorList.push(result)
                    }
                }
            }
        })
    
        if (errorList.length > 0) {
            reject(errorList)
        }
        const body = prepareReuqest(inputList, id, entityName)

        axios.post('/dynamic/send', body)
            .then(response => {
                resolve(response)
            }).catch(err => {
                reject(err)
            })
    });
};


function parseForm(nodes, fields) {
    for (const k in fields) {
        const noded = nodes.filter(node => {
            if (node.getAttribute('data-field-name') == k) {
                return node;
            }
        })

        if (noded) {
            setArrayNodesValue(noded, fields[k]);
        }
    }
}

function setArrayNodesValue(nodes, resValue) {
    nodes.forEach(node => {
        const type = node.getAttribute('type')
        if (type == 'text' || type == 'number' || type == "email") {
            node.value = resValue;
        } else if (type == 'checkbox') {
            if(Number(resValue)){
                node.checked = true
            } else {
                node.checked = false
            }
        }
        else if (type == 'radio') {
            if (node.value == resValue) {
                node.checked = true
            }
        }
        else if (type.includes('select')) {
                for (let i = 0; i < node.length; i++) {
                    if (node[i].value == resValue) {
                        node[i].selected = true
                    }
                }
        } else {
            node = null
        }
    })

}

function createReqObject(input) {
    const attributeField = input.getAttribute('data-field-name');
    if (attributeField) {
        const type = input.getAttribute('type');
        if (type == 'text' || type == 'radio') {
            return { [attributeField]: input.value };
        } else if (type == 'number') {
            return { [attributeField]: Number(input.value) }
        }
        else if (type == 'checkbox') {
            return { [attributeField]: input.checked }
        } else if(type == 'select'){
            for(let i = 0; i < input.length; i++) {
                if(input[i].selected) {
                    return { [attributeField]: input[i].value }
                }
            }
        }
        else {
            return { [attributeField]: null };
        } 
    } 

}

function prepareReuqest(inputList, id, entityName) {
    //TODO Prepare correctly req for sending data
    const body = [{ id: id, entity: entityName, fields: [] }]
    const fld = inputList.map(input => {
        return createReqObject(input);
    });

    body[0].fields.push(fld)

    return body
}

function checkRequired(errorList, input, languageCode) {
    if (input.getAttribute('required') !== null) {
        const result = requiredField(input, languageCode);
        if (result.type == "error") {
            errorList.push(result);
        }
    }
}

function checkTextMinLength(errorList, input, languageCode) {
    if (input.getAttribute('minLength') !== null) {
        let error = minLength(input, languageCode, input.getAttribute('minLength'))
        if (error.type == "error") {
            errorList.push(error);
        }
    }
}

function checkTextMaxLength(errorList, input, languageCode) {
    if (input.getAttribute('maxLength') !== null) {
        let error = maxLength(input, languageCode, input.getAttribute('maxLength'))
        if (error.type == "error") {
            errorList.push(error);
        }
    }
}

function checkMinValue(errorList, input, languageCode) {
    if (input.getAttribute('min') !== null) {
        let error = min(input, languageCode, input.getAttribute('min'))
        if (error.type == "error") {
            errorList.push(error);
        }
    }
}

function checkMaxValue(errorList, input, languageCode) {
    if (input.getAttribute('max') !== null) {
        let error = max(input, languageCode, input.getAttribute('max'))
        if (error.type == "error") {
            errorList.push(error);
        }
    }
}
