import { baseData } from '../_constance/index'
class ReqHelper{
    static responseErrorHelper(error){
        let errorObj = { code: error, message: 'basic_error_message'};
        switch (error) {
            case 400: errorObj.message = 'api_call_error_400'; break;
            case 401: errorObj.message = 'api_call_error_401'; break;
            case 403: errorObj.message = 'api_call_error_403'; break;
            case 404: errorObj.message = 'api_call_error_404'; break;
            case 405: errorObj.message = 'api_call_error_405'; break;
            case 500: errorObj.message = 'api_call_error_500'; break;
            case 801: errorObj.message = 'basic_error_message'; break;
            case 999: errorObj.message = 'error_connection_failed'; break;
            default: //console.log(errorObj);
        }
        return errorObj;
    }

    static dateConversion(timestamp){
        if(timestamp){
            if(timestamp!==null){
                let today = new Date(timestamp*1000);
                let dd = today.getDate();
                let mm = today.getMonth() + 1;
                let yyyy = today.getFullYear();
                if (dd < 10) {
                dd = '0' + dd;
                } 
                if (mm < 10) {
                mm = '0' + mm;
                } 
                let converted = dd + '.' + mm + '.' + yyyy;
                return converted;
            }else{
                return null
            }
        }else{
            return null
        }
    }

    static valueExistsInObj(obj, value){
        if(obj!==null){
            if(typeof obj === 'object'){
                for(var key in obj) {
                    if(typeof obj[key] === 'object'){
                        let exists = ReqHelper.valueExistsInObj(obj[key], value);
                        if(exists){
                            return true;
                        }
                    }else{
                        if(obj[key].toString().toLowerCase().indexOf(value.toString().toLowerCase())!==-1) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    static numberWithSpaces(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        return parts.join(".");
    }

    static isNumber(value){
        return typeof value === 'number' && 
              isFinite(value) && 
              Math.floor(value) === value;
    }
}

export default ReqHelper;