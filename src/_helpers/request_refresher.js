export const shouldRefresh = (actualTime, lastUpdatedTime, validDataTime) => {
    if (lastUpdatedTime == null) {
        return true;
    }
    if (lastUpdatedTime + validDataTime < actualTime) {
        return true;
    }

    return false;
}