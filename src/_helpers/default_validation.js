import {ValidationResult} from './Error';


import {REQUIRED_MESSAGE,REQUIRED_FIELD_MESSAGE_PL, REQUIRED_FIELD_MESSAGE_EN,
        TEXT_MIN_LENGTH, MIN_LENGTH_MESSAGE_PL, MIN_LENGTH_MESSAGE_EN,
        TEXT_MAX_LENGTH, MAX_LENGTH_MESSAGE_PL, MAX_LENGTH_MESSAGE_EN,
        NUMBER_MIN_LENGTH, MIN_MESSAGE_PL, MIN_MESSAGE_EN,
        NUMBER_MAX_LENGTH, MAX_MESSAGE_PL, MAX_MESSAGE_EN} from './messages'

/**
 * DEFAULT  VALIDATORS
 */

 export function requiredField (input, languageCode) {
    if(!input.value){
        const message = createMessage(languageCode, REQUIRED_MESSAGE)
        return new ValidationResult("error", message, input)
    }
    return new ValidationResult("success", "")
 }

 /**
  * TEXT VALIDATORS
  */
 export function minLength(input, languageCode, minLength) {
     if(input.value.length < minLength){
        const message = createMessage(languageCode, TEXT_MIN_LENGTH, minLength);
        return new ValidationResult("error", message, input)
     }
     return new ValidationResult("success", "")
 }

 export function maxLength(input, languageCode, maxLength) {
    if(input.value.length > maxLength){
       const message = createMessage(languageCode, TEXT_MAX_LENGTH, maxLength);
       return new ValidationResult("error", message, input)
    }
    return new ValidationResult("success", "")
}

/**
 * NUMBER VALIDATION
 */

export function min(input, languageCode, minLength) {
    if(input.value < minLength){
       const message = createMessage(languageCode, NUMBER_MIN_LENGTH, minLength);
       return new ValidationResult("error", message, input)
    }
    return new ValidationResult("success", "")
}

export function max(input, languageCode, maxLength) {
    if(input.value > maxLength){
       const message = createMessage(languageCode, NUMBER_MAX_LENGTH, maxLength);
       return new ValidationResult("error", message, input)
    }
    return new ValidationResult("success", "")
}

/** --------------------------------------------------------------------------------------------------------------
 *  PRIVATE FUNCTIONS
 * ---------------------------------------------------------------------------------------------------------------
 * 
 */


 function createMessage(languageCode, messageType, value) {
     switch(messageType) {
         case REQUIRED_MESSAGE:
            return resolveRequiredMessage(languageCode)
        case TEXT_MIN_LENGTH:
            return resolveMinTextMessage(languageCode, value)
        case TEXT_MAX_LENGTH:
            return resolveMaxTextMessage(languageCode, value)
        case NUMBER_MIN_LENGTH:
            return resolveMinMessage(languageCode, value)
        case NUMBER_MAX_LENGTH:
            return resolveMaxMessage(languageCode, value)
        default:
            return null
     }
 }

 function resolveRequiredMessage(langCode) {
    switch(langCode){
        case "PL":
            return REQUIRED_FIELD_MESSAGE_PL    
        case "EN":
            return REQUIRED_FIELD_MESSAGE_EN
        default: 
            return REQUIRED_FIELD_MESSAGE_PL
    }
 }

 function resolveMinTextMessage(langCode, minValue) {
    switch(langCode){
        case "PL":
            return MIN_LENGTH_MESSAGE_PL + minValue 
        case "EN":
            return MIN_LENGTH_MESSAGE_EN + minValue 
        default: 
            return MIN_LENGTH_MESSAGE_EN + minValue 
    }
 }

 function resolveMaxTextMessage(langCode, maxValue) {
    switch(langCode){
        case "PL":
            return MAX_LENGTH_MESSAGE_PL + maxValue 
        case "EN":
            return MAX_LENGTH_MESSAGE_EN + maxValue 
        default: 
            return MAX_LENGTH_MESSAGE_EN + maxValue 
    }
 }

 function resolveMinMessage(langCode, minValue) {
    switch(langCode){
        case "PL":
            return MIN_MESSAGE_PL + minValue 
        case "EN":
            return MIN_MESSAGE_EN + minValue 
        default: 
            return MIN_MESSAGE_EN + minValue 
    }
 }

 function resolveMaxMessage(langCode, maxValue) {
    switch(langCode){
        case "PL":
            return MAX_MESSAGE_PL + maxValue 
        case "EN":
            return MAX_MESSAGE_EN + maxValue 
        default: 
            return MAX_MESSAGE_EN + maxValue 
    }
 }