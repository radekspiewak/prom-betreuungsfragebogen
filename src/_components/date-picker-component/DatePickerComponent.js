import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { translateKey } from "../../_redux/actions/index";
import DatePicker, { registerLocale }  from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import deDe from 'date-fns/locale/de';
import enGB from 'date-fns/locale/en-GB';
registerLocale('en-GB', enGB);
registerLocale('de-De', deDe);

class DatePickerComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: this.props.timestamp,
            readOnly: this.props.readOnly,
            disabled: this.props.disabled
        };
    }

    dateConversion(timestamp, justHours){
        let myDate;
        if(timestamp){
            if(timestamp.toString().length<10){
                if(justHours){
                    myDate = new Date();
                }else{
                    myDate = new Date();
                    myDate.setUTCHours(0,0,0,0);
                }
            }else{
                if(timestamp.toString().length===10){
                    myDate = new Date(timestamp*1000)
                }else{
                    if(timestamp.toString().length===13){
                        myDate = new Date(timestamp)
                    }else{
                        if(justHours){
                            myDate = new Date();
                        }else{
                            myDate = new Date();
                            myDate.setUTCHours(0,0,0,0);
                        }
                    }
                }
            }
        }
        return myDate;
    }

    handleDateChange(event){
        this.props.onDateChange(event);
    }

    render() {
        if(this.props.withTime){
            return (
                <DatePicker
                    selected={this.dateConversion(this.props.timestamp, false)}
                    onChange={(e)=>this.handleDateChange(e)}
                    dateFormat="dd.MM.yyyy HH:mm"
                    timeCaption="time"
                    showTimeSelect
                    withPortal={this.props.withPortal}
                    timeFormat="HH:mm"
                    timeIntervals={15}
                    className="form-control"
                    readOnly = {this.state.readOnly}
                    disabled = {this.state.disabled}
                    locale={this.props.appLang==="DE"? "de-De": "en-GB"}
                />
            );
        }else{
            return (
                <DatePicker
                    selected={this.dateConversion(this.props.timestamp, true)}
                    onChange={(e)=>this.handleDateChange(e)}
                    dateFormat="dd.MM.yyyy"
                    withPortal={this.props.withPortal}
                    className="form-control"
                    readOnly = {this.state.readOnly}
                    disabled = {this.state.disabled}
                    locale={this.props.appLang==="DE"? "de-De": "en-GB"}
                />
            );
        }
    }
}
const mapStateToProps = state => {
    return {
        appLang: state.lang.appLanguage
    };
  }
  
const mapDispatchToProps = dispatch => ({
    translateKey: (firstLvl, secondLvl) => dispatch(translateKey(firstLvl, secondLvl)),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DatePickerComponent));