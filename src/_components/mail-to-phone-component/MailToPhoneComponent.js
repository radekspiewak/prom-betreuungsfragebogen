import * as React from 'react';

export class MailToPhoneComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: this.props.phone
        };
    }

    generateMailTo(phone){
        if(phone){
            if(this.validatePhone(phone)){
                return <a href={"tel:"+phone}>{phone}</a>
            }else{
                return phone;
            }
        }else{
            return phone;
        }
    }

    validatePhone(phone){
        if(phone.length>6){
            return true;
        }else{
            return false
        }
    }

    render() {
        const aComponent = this.generateMailTo(this.props.phone);
        return (
            <span>{aComponent}</span>
         );
    }
}