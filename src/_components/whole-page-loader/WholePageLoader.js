import * as React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'

export class WholePageLoader extends React.Component {
    render() {
        return (
            <div className="whole-page-loader">
                <FontAwesomeIcon icon={faSpinner} size="4x" className="spinner-prom" />
            </div>
        );
    }
}