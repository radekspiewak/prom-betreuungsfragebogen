import axios from 'axios';
import ReqHelper from '../../_helpers/request_helper';
import {baseData} from '../../_constance/base_data';

/**
 * Create axios instance which just handle errors.
 */

const request = axios.create({
    baseURL: baseData.base_api
});

request.interceptors.response.use(response => {
    return response;
}, error => {
    return Promise.reject(ReqHelper.responseErrorHelper(error));
});


export default request;