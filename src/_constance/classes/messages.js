/**
 * REQUIRED
 */
export const REQUIRED_MESSAGE = "REQUIRED_MESSAGE"

export const REQUIRED_FIELD_MESSAGE_PL = "Pole jest wymagane";
export const REQUIRED_FIELD_MESSAGE_EN = "Field is required";


export const TEXT_MIN_LENGTH = "TEXT_MIN_LENGTH"
export const MIN_LENGTH_MESSAGE_PL = "Wartość musi być dłuższa od ";
export const MIN_LENGTH_MESSAGE_EN = "Value must be longer then  ";

export const TEXT_MAX_LENGTH = "TEXT_MAX_LENGTH"
export const MAX_LENGTH_MESSAGE_PL = "Wartość musi być krótsza od ";
export const MAX_LENGTH_MESSAGE_EN = "Value must be shorter then ";

export const NUMBER_MIN_LENGTH = "NUMBER_MIN_LENGTH"
export const MIN_MESSAGE_PL = "Wartość musi być większa od ";
export const MIN_MESSAGE_EN = "Value must be greater then ";

export const NUMBER_MAX_LENGTH = "NUMBER_MAX_LENGTH"
export const MAX_MESSAGE_PL = "Wartość musi być mniejsza od ";
export const MAX_MESSAGE_EN = "Value must be lower then ";

