 export class ValidationResult {
    type = "success"
    message = ""
    input = null
    constructor(type, message, input){
        this.type = type
        this.message = message;
        this.input = input;
    }
}