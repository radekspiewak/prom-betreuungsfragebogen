import * as React from "react";
import { connect } from "react-redux";
import { withRouter, Prompt} from "react-router-dom";
import { translateKey, authSetActionTimestamp, getTasks, setTasksData, setClientsData, pendingClientsData, removeClientsData, setPatientClientData, 
  pendingPatientClientData, removePatientClientData, getPatientClient, getInvoices } from "../../_redux/actions/index";
import { TableProperties, NavBar, SideBar, LoaderTable, EmptyTableIcon} from "../../_components/index";
import { Alert, ClientTable } from "../../_constance/classes/index";
import { IconsMap } from "../../_helpers/index";
import { ResponseStatus } from '../../_helpers/response_status'
import axiosAuth from "../../_services/config/axios-auth";
import { ClientClientData, ClientPatientData, ClientNotesData, ClientTasksData, ClientInvoicesData }  from '../../_components/ClientDetailsComponenets/index';
//import { SortingTasks, SortingNotes, SortingPatient } from "../../_constance/classes/sortingClasses/index";

class ClientDetailsPage extends React.Component {
  alertD = Alert;
  myTimout;
  taskTimeOut;
  invoiceTimeOut;
  patientTimeout;
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "client",
      clientData: null,
      clientDataBackup: null,
      errorComponent: Alert,
      errorsMessage: null,
      errorsType: "danger",
      loadingBasics: true,
      clientRetries: 0,
      loadingPatients: true,
      notesData: [],
      loadingNotes: true,
      loadingTasks: true,
      loadingInvoices: true,
      id: 0
    };
  }

  setActiveTab(tabName){this.setState({activeTab: tabName})}

  componentDidMount() {
    document.body.classList.remove('modal-open');
    window.scrollTo(0, 0);
    this.myTimeout = setTimeout(() => {
      this.getDataBasic()
    }, 200);
  } 

  componentWillUnmount() {
      clearTimeout(this.myTimeout);
      clearTimeout(this.invoiceTimeOut);
      clearTimeout(this.notesTimeout);
      clearTimeout(this.taskTimeOut);
      clearTimeout(this.patientTimeout);
  };

  ////// BASIC CLIENT TAB DATA LOAD

  getInitialDataTime(){
      if(this.props.clientsData.length>0){
        clearTimeout(this.myTimeout);
        this.getClientDataBasic();
      }else{
        if(this.state.clientRetries<10){
          this.getClientsList();
          this.setState({clientRetries: this.state.clientRetries+1})
          this.myTimeout = setTimeout(() => {this.getInitialDataTime()}, 300);
        }else{
          this.setState({
            clientRetries: 0,
            loadingBasics: false
          })
        }
      }
  }

  getClientDataBasic() {
    const { match: { params } } = this.props;
    for(let i=0;i<this.props.clientsData.length; i++){
      if(this.props.clientsData[i].id===params.id){
        this.setState({
          loadingBasics: false,
          clientRetries: 0, 
          clientData: JSON.parse(JSON.stringify(this.props.clientsData[i])),
          id: params.id
        })
        console.log(this.props.clientsData[i]);
        this.getInitialPatient();
        this.getBasicNotes();
        this.getBasicTasksData();
        this.getBasicInvoiceData();
        break;
      }
    }
  }

  getClientsList = () => {
    if(this.props.clientsState !== ResponseStatus.PENDING){
      this.props.pendingRequestClient();
      axiosAuth.get("clients/list/0")
        .then(res => { if (res.status === 200) { return res; } else { return Promise.reject(res.status); } })
        .then(json => { return json.data })
        .then(res => {
          let mapedRes = res.map((element)=>{
            for(let i=0; i<this.props.patientData.length; i++){
              if(this.props.patientData[i].clientId===element.id){
                element.locationP = this.props.patientData[i].patientAddress ? this.props.patientData[i].patientAddress.city:'';
                element.patientName = this.props.patientData[i].name;
              }
            }
            return new ClientTable(element);}
          );
          this.props.setClientData(mapedRes);
        })
        .catch((error) => {this.props.clearDataClient();});
    }
  }

  getPatientList(){
    this.props.pendingPatientData();
    axiosAuth.get("patients/list/CLIENT/0/"+this.props.appLang)
      .then(res => { if (res.status === 200) { return res; } else { return Promise.reject(res.status); } })
      .then(json => { return json.data })
      .then(res => {
        this.props.setPatientData(res);
        this.getInitialDataTime();
      })
      .catch((error) => {
        this.props.clearPatientData();
        this.getInitialDataTime();
      });
  }

  getDataBasic() {
    this.setState({loadingBasics: true});
    if (this.props.patientData.length === 0) {
      if (this.props.patientData.length !== 0 && this.props.patientData !== undefined) {
        this.getInitialDataTime();
      } else {
        this.getPatientList();
      }
    } else {
      this.getInitialDataTime();
    }
  }


















  ///// BASIC PATIENT TAB DATA LOAD

  getInitialPatient(){
    if(this.props.patientClientStatus === ResponseStatus.READY){
      clearTimeout(this.patientTimeout);
      this.setState({loadingPatients: false})
    }else{
      if(this.props.patientClientStatus === ResponseStatus.FREE){
        this.props.getPatientClientData(this.props.appLang);
        this.patientTimeout = setTimeout(() => {
          this.getInitialPatient()
        }, 500);
      }else{
        if(this.props.patientClientStatus === ResponseStatus.ERROR){
          clearTimeout(this.patientTimeout);
          this.setState({ loadingPatients: false })
        }else{
          this.patientTimeout = setTimeout(() => {
            this.getInitialPatient()
          }, 500);
        }
      }
    }
  }












  ///// BASIC NOTES TAB DATA LOAD

  getBasicNotes(){
    this.setState({loadingNotes: true})
    const { match: { params } } = this.props;
    axiosAuth.get("/notes/CLIENT/"+params.id+"/0")
      .then(res => { if (res.status === 200) { return res; } else { return Promise.reject(res.status); } })
      .then(json => { return json.data })
      .then(res => {
        this.setState({
          notesData: res,
          loadingNotes: false
        })
      })
      .catch((error) => {
        console.log(error);
        this.setState({ loadingNotes: false })
      });
  }






  ///// BASIC TASKS TAB DATA LOAD

  getBasicTasksData(){
    if(this.props.tasksStatus === ResponseStatus.READY){
      clearTimeout(this.taskTimeOut);
      this.setState({loadingTasks: false})
    }else{
      if(this.props.tasksStatus === ResponseStatus.FREE){
        this.props.getGlobalTasksData();
        this.taskTimeOut = setTimeout(() => {
          this.getBasicTasksData()
        }, 500);
      }else{
        if(this.props.tasksStatus === ResponseStatus.ERROR){
          clearTimeout(this.taskTimeOut);
          this.setState({ loadingTasks: false })
        }else{
          this.taskTimeOut = setTimeout(() => {
            this.getBasicTasksData()
          }, 500);
        }
      }
    }
  }



  ///// BASIC INVOICE TAB DATA LOAD

  getBasicInvoiceData(){
    if(this.props.invoiceStatus === ResponseStatus.READY){
      clearTimeout(this.invoiceTimeOut);
      this.setState({loadingInvoices: false})
    }else{
      if(this.props.invoiceStatus === ResponseStatus.FREE){
        this.props.getInvoicesData();
        this.invoiceTimeOut = setTimeout(() => {
          this.getBasicInvoiceData()
        }, 500);
      }else{
        if(this.props.invoiceStatus === ResponseStatus.ERROR){
          clearTimeout(this.invoiceTimeOut);
          this.setState({ loadingInvoices: false })
        }else{
          this.invoiceTimeOut = setTimeout(() => {
            this.getBasicInvoiceData()
          }, 500);
        }
      }
    }
  }



  render() {
    let ClientData;
    if (this.state.loadingBasics) {
      ClientData = <div className="flexCenterItems emptyLoaderMin"><LoaderTable /></div>
    }else{
      if(this.state.clientData===null){
        ClientData = <EmptyTableIcon RefreshMe={() => this.getDataBasic()} text={this.props.translateKey("client_does_not_exist")} />
      }else{
        ClientData = <ClientClientData clientData={this.state.clientData}/>
      }
    }
    let PatientData;
    if (this.state.loadingPatients) {
      PatientData = <div className="flexCenterItems emptyLoaderMin"><LoaderTable /></div>
    }else{
      if(this.props.patientData===[]){
        PatientData = <EmptyTableIcon RefreshMe={() => this.getInitialPatient()} text={this.props.translateKey("no_data_to_show")} />
      }else{
        PatientData = <ClientPatientData RefreshMe={() => this.getInitialPatient()} id={this.state.id}/>
      }
    }

    let NotesData;
    if (this.state.loadingNotes) {
      NotesData = <div className="flexCenterItems emptyLoaderMin"><LoaderTable /></div>
    }else{
      if(this.state.notesData===[]){
        NotesData = <EmptyTableIcon RefreshMe={() => this.getBasicNotes()} text={this.props.translateKey("no_data_to_show")} />
      }else{
        NotesData = <ClientNotesData RefreshMe={() => this.getBasicNotes()} notesData={this.state.notesData} id={this.state.id}/>
      }
    }

    let TaskData;
    if (this.state.loadingTasks) {
      TaskData = <div className="flexCenterItems emptyLoaderMin"><LoaderTable /></div>
    }else{
      if(this.props.tasksData===[]){
        TaskData = <EmptyTableIcon RefreshMe={() => this.getBasicTasksData()} text={this.props.translateKey("no_data_to_show")} />
      }else{
        TaskData = <ClientTasksData RefreshMe={() => this.getBasicTasksData()} id={this.state.id}/>
      }
    }




    let InvoiceData;
    if (this.state.loadingInvoices) {
      InvoiceData = <div className="flexCenterItems emptyLoaderMin"><LoaderTable /></div>
    }else{
      if(this.props.invoicesData===[]){
        InvoiceData = <EmptyTableIcon RefreshMe={() => this.getBasicInvoiceData()} text={this.props.translateKey("no_data_to_show")} />
      }else{
        InvoiceData = <ClientInvoicesData RefreshMe={() => this.getBasicInvoiceData()} id={this.state.id}/>
      }
    }



    return (
      <div className="mainContentWrapper" onClick={() => {this.props.tookAction()}}>
        <NavBar breadCrumbs={["leads", "LeadDetails"]} />
        <TableProperties options={[]} filterData={[]}/>
        <div className="mainContent">
          <SideBar />
          <div className="listDataWrapper transparentClass">
            <nav>
              <div className="nav nav-tabs" id="nav-tab" role="tablist">
                <a className="nav-item nav-link active nav-link-invoices" onClick={()=>{this.setActiveTab("client")}} id="nav-details-tab" data-toggle="tab" href="#nav-details" role="tab" aria-controls="nav-details" aria-selected="true">
                  {this.props.translateKey("client")}
                </a>
                <a className="nav-item nav-link nav-link-invoices" onClick={()=>{this.setActiveTab("tasks")}} id="nav-tasks-tab" data-toggle="tab" href="#nav-tasks" role="tab" aria-controls="nav-tasks" aria-selected="false">
                  {this.props.translateKey("tasks")}
                </a>
                <a className="nav-item nav-link nav-link-invoices" onClick={()=>{this.setActiveTab("carers")}} id="nav-carers-tab" data-toggle="tab" href="#nav-carers" role="tab" aria-controls="nav-carers" aria-selected="false">
                  {this.props.translateKey("carers")}
                </a>
                <a className="nav-item nav-link nav-link-invoices" onClick={()=>{this.setActiveTab("contracts")}} id="nav-contracts-tab" data-toggle="tab" href="#nav-contracts" role="tab" aria-controls="nav-contracts" aria-selected="false">
                  {this.props.translateKey("contracts")}
                </a>
                <a className="nav-item nav-link nav-link-invoices" onClick={()=>{this.setActiveTab("invoices")}} id="nav-invoices-tab" data-toggle="tab" href="#nav-invoices" role="tab" aria-controls="nav-invoices" aria-selected="false">
                  {this.props.translateKey("invoices")}
                </a>
                <a className="nav-item nav-link nav-link-invoices" onClick={()=>{this.setActiveTab("notes")}} id="nav-notes-tab" data-toggle="tab" href="#nav-notes" role="tab" aria-controls="nav-notes" aria-selected="false">
                  {this.props.translateKey("notes")}
                </a>
                <a className="nav-item nav-link nav-link-invoices" onClick={()=>{this.setActiveTab("patients")}} id="nav-patients-tab" data-toggle="tab" href="#nav-patients" role="tab" aria-controls="nav-patients" aria-selected="false">
                  {this.props.translateKey("patients")}
                </a>
              </div>
            </nav>
            <div className="tab-content position-relative" id="nav-tabContent">
              <div className="tab-pane fade show active" id="nav-details" role="tabpanel" aria-labelledby="nav-details-tab">
                <div className="leadSection">
                  {ClientData}
                </div>
              </div>
              <div className="tab-pane fade" id="nav-tasks" role="tabpanel" aria-labelledby="nav-tasks-tab">
                <div className="leadSection">
                  {TaskData}
                </div>
              </div>
              <div className="tab-pane fade" id="nav-carers" role="tabpanel" aria-labelledby="nav-careres-tab">
                <div className="leadSection">
                  {ClientData}
                </div>
              </div>
              <div className="tab-pane fade" id="nav-contracts" role="tabpanel" aria-labelledby="nav-contracts-tab">
                <div className="leadSection">
                  {ClientData}
                </div>
              </div>
              <div className="tab-pane fade" id="nav-invoices" role="tabpanel" aria-labelledby="nav-invoices-tab">
                <div className="leadSection">
                  {InvoiceData}
                </div>
              </div>
              <div className="tab-pane fade" id="nav-notes" role="tabpanel" aria-labelledby="nav-notes-tab">
                <div className="leadSection">
                  {NotesData}
                </div>
              </div>
              <div className="tab-pane fade" id="nav-patients" role="tabpanel" aria-labelledby="nav-patients-tab">
                <div className="leadSection">
                  {PatientData}
                </div>
              </div>
              <div className="basic-bottom-section-line" style={{backgroundImage: "url(" + IconsMap.svg.pasek_big + ")"}}></div>
            </div>
          </div>
        </div>
        <Prompt
            when={false}
            message={this.props.translateKey("unsaved_data_lost")}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    appLang: state.lang.appLanguage,
    i18: state.lang.i18data,
    clientsData: state.database.clients,
    patientData: state.database.patientClient,
    patientClientStatus: state.database.patientClientState,
    tasksData: state.database.tasks,
    tasksStatus: state.database.tasksState,
    invoicesData: state.database.invoices,
    invoiceStatus: state.database.invoicesState,
  };
};

const mapDispatchToProps = dispatch => ({
  translateKey: (firstLvl, secondLvl) => dispatch(translateKey(firstLvl, secondLvl)),
  setClientData: (data) => dispatch(setClientsData(data)),
  pendingRequestClient: () => dispatch(pendingClientsData()),
  clearDataClient: () => dispatch(removeClientsData()),

  getPatientClientData: (lang) => dispatch(getPatientClient(lang)),
  setPatientData: (data) => dispatch(setPatientClientData(data)),
  pendingPatientData: () => dispatch(pendingPatientClientData()),
  clearPatientData: () => dispatch(removePatientClientData()),

  tookAction: () => dispatch(authSetActionTimestamp()),

  getGlobalTasksData: () => dispatch(getTasks()),
  setGlobalTasksData: (data) => dispatch(setTasksData(data)),
  
  getInvoicesData: () => dispatch(getInvoices()),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ClientDetailsPage));