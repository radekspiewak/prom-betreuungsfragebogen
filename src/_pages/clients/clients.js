import * as React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { translateKey, setClientsData, pendingClientsData, removeClientsData, authSetActionTimestamp, setPatientClientData, pendingPatientClientData, removePatientClientData } from "../../_redux/actions/index";
import { TableColumHead, TableProperties, LoaderTable, NavBar, SideBar, EmptyTableIcon, AlertMessage, TableDateFormat, TablePagination } from "../../_components/index";
import axiosAuth from "../../_services/config/axios-auth";
import { TableEnums } from "../../_constance/enums";
import { Alert, ClientTable, Paging } from "../../_constance/classes/index";
import { ReqHelper, IconsMap } from "../../_helpers/index";
import { SortingClients } from "../../_constance/classes/sortingClasses/index";

class ClientsMainPage extends React.Component {
  alertD = Alert;
  myTimout;
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: false,
      sorting: SortingClients,
      searchQuery: null,
      filterData: [],
      errorComponent: Alert,
      pagingItem: Paging
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    this.getDataBasic();
  }

  componentWillUnmount() {
    clearTimeout(this.myTimeout);
  };

  _prepareFilterObj() {
    let filterData = [];
    filterData.push({
      filterName: "carer",
      filterOptions: [
        {
          column: this.props.translateKey('trip_ends_in_7_days'),
          active: false,
          type: "dateFrom"
        },
      ]
    });
    return filterData;
  }

  getClientsList = () => {
    this.props.pendingRequest();
    axiosAuth.get("clients/list/0")
      .then(res => { if (res.status === 200) { return res; } else { return Promise.reject(res.status); } })
      .then(json => { return json.data })
      .then(res => {
        let mapedRes = res.map((element)=>{
          for(let i=0; i<this.props.patientData.length; i++){
            if(this.props.patientData[i].clientId===element.id){
              element.locationP = this.props.patientData[i].patientAddress ? this.props.patientData[i].patientAddress.city:'';
              element.patientName = this.props.patientData[i].name;
            }
          }
          return new ClientTable(element);}
        );
        this.props.setData(mapedRes);
        this.setState({
          data: mapedRes,
          sorting: SortingClients,
          loading: false,
          filterData: this._prepareFilterObj()
        },() => this.filterOrDataChange());
      })
      .catch((error) => {
        this.props.clearData();
        this.alertD.show = true;
        this.alertD.type = "danger";
        this.alertD.message = this.props.translateKey("basic_error_message");
        this.setState({
          data: [],
          loading: false,
          errorComponent: this.alertD
        },() => this.filterOrDataChange());
        console.log(error);
        this.myTimeout = setTimeout(() => {
          this.setState({ errorComponent: Alert });
        }, 3000);
      });
  }

  getPatientList(){
    this.props.pendingPatientData();
    axiosAuth.get("patients/list/CLIENT/0/"+this.props.appLang)
      .then(res => { if (res.status === 200) { return res; } else { return Promise.reject(res.status); } })
      .then(json => { return json.data })
      .then(res => {
        this.props.setPatientData(res);
        this.getClientsList();
      })
      .catch((error) => {
        console.log(error);
        this.props.clearPatientData();
        this.getClientsList();
      });
  }

  getDataBasic() {
    this.setState({ loading: true })
    if (this.props.patientData.length === 0) {
      if (this.props.patientData.length !== 0 && this.props.patientData !== undefined) {
        this.getClientsList();
      } else {
        this.getPatientList();
      }
    } else {
      this.getClientsList();
    }
  }

  filterOrDataChange(val){
    let finalDataToShow = this.propsFilterSearch(val);
    this.propsSortingData(finalDataToShow);
    this.propsPagingData(finalDataToShow);
    let finalData = finalDataToShow.slice((this.state.pagingItem.currentPage-1)*this.state.pagingItem.itemsPerPage, this.state.pagingItem.currentPage*this.state.pagingItem.itemsPerPage);
    this.setState({data: finalData})
  }

  propsSearchTable(val) {
    this.setState({ searchQuery: val.trim() });
    this.filterOrDataChange(val);
  }

  propsSortType(type){
    let sortingObj = [];
    for(let i=0; i<this.state.sorting.length; i++){
      if(this.state.sorting[i].column===type){
        if(this.state.sorting[i].active===true){
          let item = this.state.sorting[i];
          item.type==='ASC'? item.type="DESC" : item.type='ASC';
          sortingObj.push(item);
        }else{
          let item = this.state.sorting[i];
          item.type='ASC';
          item.active = true;
          sortingObj.push(item);
        }
      }else{
        let item = this.state.sorting[i];
        item.type='ASC';
        item.active = false;
        sortingObj.push(item);
      }
    }
    this.setState({sorting: sortingObj});
    this.filterOrDataChange();
  }

  propsFilterSearch = (val) => {
    let finalDataAfter = [];
    if (this.props.clientsData !== null && this.props.clientsData.length > 0) {
      if(val && val!==null){
        // searching
        let filters = ReqHelper.getMeFilersObj(this.state.filterData);
        if(filters.length>0){
          // mamy search i filtry
          // search filter
          let afterSearchData = this.props.clientsData.filter((value) => {
            return ReqHelper.valueExistsInObj(value, val);
          });
          finalDataAfter = afterSearchData.filter((value) => {
            let gotIt = true;
            for(let i=0; i<filters.length; i++){
              if(filters[i].type==="dateFrom"){
                let DateMinus7 = new Date();
                DateMinus7.setDate(DateMinus7.getDate()-7);
                if((value.carerC*1000)>DateMinus7.getTime()){
                  gotIt = true;
                  break;
                }else{
                  gotIt = false;
                }
              }
            }
            return gotIt;
          });
        }else{
          // samo search bez filtrów
          finalDataAfter = this.props.clientsData.filter((value) => {
            return ReqHelper.valueExistsInObj(value, val);
          });
        }
      }else{
        let search = this.state.searchQuery;
        if(search && search!==null && val!==''){
          // filtrowanie z wpisanym search
          let filters = ReqHelper.getMeFilersObj(this.state.filterData);
          if(filters.length>0){
            // mamy search i filtry
            let afterSearchData = this.props.clientsData.filter((value) => {
              return ReqHelper.valueExistsInObj(value, search);
            });
            finalDataAfter = afterSearchData.filter((value) => {
              let gotIt = true;
              for(let i=0; i<filters.length; i++){
                if(filters[i].type==="dateFrom"){
                  let DateMinus7 = new Date();
                  DateMinus7.setDate(DateMinus7.getDate()-7);
                  if((value.carerC*1000)>DateMinus7.getTime()){
                    gotIt = true;
                    break;
                  }else{
                    gotIt = false;
                  }
                }
              }
              return gotIt;
            });
          }else{
            // samo search bez filtrów
            finalDataAfter = this.props.clientsData.filter((value) => {
              return ReqHelper.valueExistsInObj(value, search);
            });
          }
        }else{
          //samo filtrowanie
          let filters = ReqHelper.getMeFilersObj(this.state.filterData);
          if(filters.length>0){
            finalDataAfter = this.props.clientsData.filter((value) => {
              let gotIt = true;
              for(let i=0; i<filters.length; i++){
                if(filters[i].type==="dateFrom"){
                  let DateMinus7 = new Date();
                  DateMinus7.setDate(DateMinus7.getDate()-7);
                  if((value.carerC*1000)>DateMinus7.getTime()){
                    gotIt = true;
                    break;
                  }else{
                    gotIt = false;
                  }
                }
              }
              return gotIt;
            });
          }else{
            finalDataAfter = this.props.clientsData;
          }
        }
      }
    }
    return finalDataAfter;
  }

  propsSortingData(data){
    let sortMeBy;
    for(let i=0; i<this.state.sorting.length; i++){
        if(this.state.sorting[i].active===true){
          sortMeBy = this.state.sorting[i];
        }
    }
    if(sortMeBy){
      if(sortMeBy.format === "text"){
        data.sort((a,b)=>{
          let nameA=a[sortMeBy.column], nameB=b[sortMeBy.column];
          if(!ReqHelper.isNumber(parseInt(a[sortMeBy.column]))){
            if(a[sortMeBy.column]){
              nameA=a[sortMeBy.column].toLowerCase()
            }
            if(nameB){
              nameB=b[sortMeBy.column].toLowerCase();
            }
          }
          if(sortMeBy.type!=="ASC"){
            if (nameA === null) {return -1;}
            else if (nameB === null) {return 1;}
            else if (nameA === nameB) {return 0;}
            if (nameA.localeCompare(nameB)===-1){ return -1; }
            else{ return 1; }
          }else{
            if (nameA === null) {return 1;}
            else if (nameB === null) {return -1;}
            else if (nameA === nameB) {return 0;}
            if (nameB.localeCompare(nameA)===-1){return -1;}
            else{ return 1; }
          }
        })
      }else{
        if(sortMeBy.format === "date"){
          data.sort((a,b)=>{
            let nameA=a[sortMeBy.column]*1000, nameB=b[sortMeBy.column]*1000;
            if(sortMeBy.type!=="ASC"){
              if (nameA === null) {return -1;}
              else if (nameB === null) {return 1;}
              else if (nameA === nameB) {return 0;}
              if (nameA < nameB){ return -1; }
              else{ return 1; }
            }else{
              if (nameA === null) {return 1;}
              else if (nameB === null) {return -1;}
              else if (nameA === nameB) {return 0;}
              if (nameA > nameB){return -1;}
              else{ return 1; }
            }
          })
        }
      }
    }
    return data;
  }

  propsPagingData(data){
    if(data.length<=25){
      this.setState(prevState => {
        return {
          ...prevState,
          pagingItem: {
            totalItems: data.length,
            itemsPerPage: prevState.pagingItem.itemsPerPage,
            currentPage: 1,
            visiblePages: 5,
            pageFrom: 1,
            pageTo: 5,
            totalPages: Math.ceil(data.length/prevState.pagingItem.itemsPerPage)
          }
        }
      });
    }else{
      this.setState(prevState => {
        return {
          ...prevState,
          pagingItem: {
            ...prevState.pagingItem,
            totalItems: data.length,
            pageFrom: 1,
            pageTo: 5,
            currentPage: prevState.pagingItem.currentPage>Math.ceil(data.length/prevState.pagingItem.itemsPerPage)? Math.ceil(data.length/prevState.pagingItem.itemsPerPage) : prevState.pagingItem.currentPage,
            totalPages: Math.ceil(data.length/prevState.pagingItem.itemsPerPage)
          }
        }
      });
    }
  }

  navigateToDetails(clientData){
    this.props.history.push('/clientDetails/'+clientData.id)
  }

  render() {
    let emptyHandler;
    let PreparedTableContent;
    if (this.state.loading) {
      emptyHandler = <LoaderTable />
    } else {
      if (this.props.clientsData === null) {
        emptyHandler = <EmptyTableIcon RefreshMe={() => this.getDataBasic()} text={this.props.translateKey("no_data")} />
      } else {
        if (this.props.clientsData.length === 0) {
          emptyHandler = <EmptyTableIcon RefreshMe={() => this.getDataBasic()} text={this.props.translateKey("no_data_to_show")} />
        }else{
          if(this.state.data.lengh===0){
            emptyHandler = <EmptyTableIcon RefreshMe={() => this.getDataBasic()} text={this.props.translateKey("no_data_to_show")} />
          }else{
            PreparedTableContent = this.state.data.map(element => (
              <tr className="table-row-wrapper cursor-pointer" key={element.id} onClick={()=>{this.navigateToDetails(element)}}>
                <td className={"table-cell-wrapper " + (element.client !== null ? "got-data" : 'no-data')}>{element.client}</td>
                <td className={"table-cell-wrapper " + (element.patientName !== null ? "got-data" : 'no-data')}>{element.patientName}</td>
                <td className={"table-cell-wrapper " + (element.locationC !== null ? "got-data" : 'no-data')}>{element.locationC}</td>
                <td className={"table-cell-wrapper " + (element.locationP !== null ? "got-data" : 'no-data')}>{element.locationP}</td>
                <td className={"table-cell-wrapper " + (element.carerC !== null ? "got-data" : 'no-data')}><TableDateFormat timestamp={element.carerC}/></td>
              </tr>
            ))
          }
        }
      }
    }
    let alertComponent = <AlertMessage options={this.state.errorComponent}></AlertMessage>
    return (
      <div className="mainContentWrapper" onClick={() => this.props.tookAction()}>
        <NavBar breadCrumbs={["clients"]} />
        <TableProperties onShowMeData={()=>this.filterOrDataChange()} onSearch={(val) => this.propsSearchTable(val)} options={[TableEnums.SEARCH, TableEnums.FILTER]} filterData={this.state.filterData} sortData={this.state.sorting} />
        <div className="mainContent">
          <SideBar />
          <div className="listDataWrapper">
            <div className="listData">
              <table className="table">
                <thead>
                  <tr>
                    <TableColumHead sorting={this.state.sorting} name={this.props.translateKey("client")} onSortType={(data) => this.propsSortType(data)} type="client" />
                    <TableColumHead sorting={this.state.sorting} name={this.props.translateKey("patient")} onSortType={(data) => this.propsSortType(data)} type="patientName" />
                    <TableColumHead sorting={this.state.sorting} name={this.props.translateKey("location") + ' ' + this.props.translateKey("client")} onSortType={(data) => this.propsSortType(data)} type="locationC" />
                    <TableColumHead sorting={this.state.sorting} name={this.props.translateKey("location") + ' ' + this.props.translateKey("patient")} onSortType={(data) => this.propsSortType(data)} type="locationP" />
                    <TableColumHead sorting={this.state.sorting} name={this.props.translateKey("carer_trip_end") + ' ' + this.props.translateKey("client")} onSortType={(data) => this.propsSortType(data)} type="carerC" />
                  </tr>
                </thead>
                <tbody>{PreparedTableContent}</tbody>
              </table>
              {emptyHandler}
              {alertComponent}
              <TablePagination recalculateAll={()=>this.filterOrDataChange()} simpleDataUpdate={()=>this.filterOrDataChange()}  loading={this.state.loading} paginationObj={this.state.pagingItem} totalItems={this.state.pagingItem.totalItems}/>
            </div>
            <div className="basic-bottom-section-line" style={{backgroundImage: "url(" + IconsMap.svg.pasek_big + ")"}}></div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    appLang: state.lang.appLanguage,
    i18: state.lang.i18data,
    clientsData: state.database.clients,
    clientsTimeStamp: state.database.clientsTimeStamp,
    patientData: state.database.patientClient,
    patientTimeStamp: state.database.patientClientTimeStamp
  };
};

const mapDispatchToProps = dispatch => ({
  translateKey: (firstLvl, secondLvl) => dispatch(translateKey(firstLvl, secondLvl)),
  setData: (data) => dispatch(setClientsData(data)),
  pendingRequest: () => dispatch(pendingClientsData()),
  clearData: () => dispatch(removeClientsData()),
  tookAction: () => dispatch(authSetActionTimestamp()),
  setPatientData: (data) => dispatch(setPatientClientData(data)),
  pendingPatientData: () => dispatch(pendingPatientClientData()),
  clearPatientData: () => dispatch(removePatientClientData()),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ClientsMainPage));