import * as React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { translateKey } from "../../_redux/actions/index";
import { WholePageLoader } from "../../_components/index";
import Button from 'react-bootstrap/Button';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';

class PromedicaFormPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      currentUser: 0,
      currentSection: 0
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    this.setState({loader: false})
    const { match: { params } } = this.props;
    this.setState({
      currentUser: params.id,
      currentSection: parseInt(params.section)
    })
    //TODO RECEIVE SOME BASIC DATA
  }

  previousPageClick = () =>{
    this.setState({
      currentSection: parseInt(this.state.currentSection)-1
    })
  }

  nextPageClick = () =>{
    this.setState({
      currentSection: parseInt(this.state.currentSection)+1
    })
  }

  render() {
    const { loader } = this.state;
    let content = (
      <WholePageLoader />
    )
    if(!loader){
      console.log(this.state.currentSection);
      switch(this.state.currentSection){
        case 0:   content = (
                    <div className="section-class-container">
                        {this.props.translateKey("bb_client_data_p_0")}
                    </div>
                  );
                  break;
        case 1:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_lead_data_p_1")}
                    </div>
                  );
                  break;
        case 2:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_contact_data_p_2")}
                    </div>
                  );
                  break;
        case 3:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_pflegegrad_data_p_3")}
                    </div>
                  );
                  break;
        case 4:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_ills_data_p_4")}
                    </div>
                  );
                  break;
        case 5:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_comnunication_data_p_5")}
                    </div>
                  );
                  break;
        case 6:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_orientation_data_p_6")}
                    </div>
                  );
                  break;
        case 7:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_characteristic_data_p_7")}
                    </div>
                  );
                  break;
        case 8:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_movement_data_p_8")}
                    </div>
                  );
                  break;
        case 9:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_defecation_data_p_9")}
                    </div>
                  );
                  break;
        case 10:   content = (
                    <div className="section-class-container">
                        {this.props.translateKey("bb_hugiene_data_p_10")}
                    </div>
                  );
                  break;
        case 11:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_foods_data_p_11")}
                    </div>
                  );
                  break;
        case 12:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_clothes_data_p_12")}
                    </div>
                  );
                  break;
        case 13:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_sleep_data_p_13")}
                    </div>
                  );
                  break;
        case 14:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_pflegedienst_data_p_14")}
                    </div>
                  );
                  break;
        case 15:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_therapy_data_p_15")}
                    </div>
                  );
                  break;
        case 16:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_expectations_data_p_16")}
                    </div>
                  );
                  break;
        case 17:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_actions_data_p_17")}
                    </div>
                  );
                  break;
        case 18:   content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_place_data_p_18")}
                    </div>
                  );
                  break;
        default:  content = (
                    <div className="section-class-container">
                      {this.props.translateKey("bb_client_data_p_0")}
                    </div>
                  );
      }
    }
    return (
      <div className="wrongContentWrapper">
        <div className="wrongContainerWrapper">
          {content}
          <ButtonToolbar className="btn-toolbar-fixed">
            {this.state.currentSection!==0?
              <Button variant="primary" className="cursor-pointer next-previous-class" onClick={this.previousPageClick}>{this.props.translateKey("previous")}</Button>
              :
              ''
            }
            {this.state.currentSection!==18?
              <Button variant="primary" className="cursor-pointer next-previous-class" onClick={this.nextPageClick}>{this.props.translateKey("next")}</Button>
              :
              ''
            }
          </ButtonToolbar>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    i18: state.lang.i18data,
  };
};

const mapDispatchToProps = dispatch => ({
  translateKey: (firstLvl, secondLvl) => dispatch(translateKey(firstLvl, secondLvl))
});

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(PromedicaFormPage));