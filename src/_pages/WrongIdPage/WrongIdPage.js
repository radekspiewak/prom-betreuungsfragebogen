import * as React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { translateKey } from "../../_redux/actions/index";
import { SimpleLoader } from "../../_components/index";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faTimes} from '@fortawesome/free-solid-svg-icons'

class WrongIdPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {loader: true};
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    this.setState({loader: false})
  }

  render() {
    const { loader } = this.state;
    let content = (
      <SimpleLoader />
    )
    if(!loader){
      content = (
        <div className="wrong-id-class-container">
          <FontAwesomeIcon icon={faTimes} size="6x" className="spinner-prom" />
          <div className="wrong-id-class-message">{this.props.translateKey("bb_empty_or_wrong_client")}</div>
        </div>
      )
    }
    return (
      <div className="wrongContentWrapper">
        <div className="container wrongContainerWrapper">
          {content}
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({translateKey: (firstLvl, secondLvl) => dispatch(translateKey(firstLvl, secondLvl))});
export default withRouter(connect(null,mapDispatchToProps)(WrongIdPage));