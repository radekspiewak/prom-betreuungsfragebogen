
import { REFRESH_PENDING, REFRESH_FINISHED } from "../actions/action-types";
const initialState = {
  refreshing: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case REFRESH_PENDING:
        return {
          refreshing: true
        };
    case REFRESH_FINISHED:
        return {
          refreshing: false
        };
    default:
      return state;
  }
};

export default reducer;