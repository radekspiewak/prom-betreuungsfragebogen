import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { combineReducers } from 'redux';
import languagesReducer from './reducers/language-reducer';
import authReducer from './reducers/auth';
import refreshTokenReducer from './reducers/refresh-status';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
  lang: languagesReducer,
  auth: authReducer,
  refreshTokenStatus: refreshTokenReducer,
});

const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(thunk)
));

export default store;