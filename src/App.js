import React, { Component } from 'react';
import { Switch, BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import WrongIdPage from './_pages/WrongIdPage/WrongIdPage';
import PromedicaFormPage from './_pages/promedica-form-page/PromedicaFormPage';
import { getBasicSelectedLang } from './_redux/actions/index';
import { connect } from 'react-redux';
require('es6-promise/auto');

class Root extends Component {
    componentDidMount() {
        this.props.getBasicSelectedLang();
    }
    render() {
        let routes = (
            <Switch>
                <Route exact path="/" component={WrongIdPage} />
                <Route exact path="/:id/:section?" component={PromedicaFormPage} />
                <Redirect to='/' />
            </Switch>
        );
        return (
            <Router>
                {routes}
            </Router>
        );
    }
}

const mapDispatchToProps = dispatch => {return {getBasicSelectedLang: () => dispatch(getBasicSelectedLang())}};
const mapStateToProps = (state) => {return {langCode: state.lang.appLanguage}};
export default connect(mapStateToProps, mapDispatchToProps)(Root);